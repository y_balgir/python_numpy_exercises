import numpy as np  #Numpy_Exercise1
def Numpy_Exercise_2():
    print np.__version__
    np.show_config()    
    return 

def Numpy_Exercise_3():    
    nullArray = np.empty(10)
    print nullArray    
    return 

def Numpy_Exercise_4():    
    print help(np.add)    
    return 

def Numpy_Exercise_5():    
    nullVector = np.empty(10)
    nullVector[4] = 1
    print  nullVector
    return 

def Numpy_Exercise_6():    
    rangeVector = np.arange(10,50)       
    print  rangeVector
    return 


def Numpy_Exercise_7():    
    rangeVector = np.arange(10,50)       
    #rangeVector = np.fliplr([rangeVector])[0]
    rangeVector = rangeVector[::-1]
    print  rangeVector
    return 

def Numpy_Exercise_8():    
    rangeMatrix = np.arange(0,9).reshape(3,3)   #Correct Answer
    
    #rangeMatrix = np.matrix('1 2; 3 4')
    #rangeMatrix = np.array(range(25)).reshape((5, 5))
    #rangeMatrix = np.ndarray((3, 3))
    print  rangeMatrix
    return 

def Numpy_Exercise_9():    
    rangeMatrix = np.array([1,2,0,0,4,0])    
    rangeMatrix = np.nonzero(rangeMatrix) #Correct answer
    
    #This will identify the zero indices
    #rangeMatrix = np.argwhere(rangeMatrix==0).flatten()
    #rangeMatrix = np.nonzero(rangeMatrix == 0)[0] #Correct answer
    print  rangeMatrix
    return 

def Numpy_Exercise_10():    
    #iMatrix = np.identity(3)
    iMatrix = np.eye(3) # try (3,5)
    print  iMatrix
    return 

def Numpy_Exercise_11():        
    eMatrix = np.empty((3,3,3))
    #eMatrix = np.empty([3,3,3]) #Exactly the same outcome
    print  eMatrix 
    return 


def Numpy_Exercise_12():    
    eMatrix = np.random.random((10,10))
    zMin,zMax = eMatrix.min(),eMatrix.max()
    print  zMin,zMax
    return 

def Numpy_Exercise_13():    
    eMatrix = np.random.random(30)
    avg1 = np.average(eMatrix)
    avg2 = eMatrix.mean()    
    print  avg1,avg2
    return 

def Numpy_Exercise_14():        
    eMatrix = np.ones((5,5))
    
    eMatrix[1:-1,1:-1] = 0    
    #eMatrix[1:4,1:4] = 0
    #eMatrix = np.zeros(9).reshape((3,3))
    #eMatrix = np.zeros((5,5))
    #eMatrix[0] = 1
    #eMatrix[4] = 1
    #eMatrix[:,0] = 1
    #eMatrix[:,4] = 1
    print  eMatrix
    return 

def Experiments_2d_Array():
    arr = [[]]    
    arr.append("aa1")    
    arr[0].append("aa2")
    arr[0].append("aa3")
    arr.append("aa4")    
    #arr[1].append("bb1")
    #arr[1].append("bb2")
    #arr[1].append("bb3")
    print  arr
    return


def Numpy_Exercise_15():        
    print  0 * np.nan       #nan
    print  np.nan == np.nan #false
    print  np.inf > np.nan  #false
    print  np.nan - np.nan  #nan
    print  0.3 == 3 * 0.1   #false
    return 

def Numpy_Exercise_16():    #good one
    iMatrix = np.diag(np.arange(1,5),-1)
    print iMatrix
    return

def Numpy_Exercise_17():    
    iMatrix = np.zeros((8,8),dtype=int)
    iMatrix[1::2,::2]=1

    print iMatrix
    return




















